// soal 1
console.log("=======\nSOAL 1\n=======");

var nilai = 1;

console.log("LOOPING PERTAMA");
while (nilai <= 20) {
  if (nilai % 2 == 0) {
    console.log(nilai + " " + "I love coding");
  }
  nilai++;
}
console.log("LOOPING KEDUA");
while (nilai > 0) {
  if (nilai % 2 == 0) {
    console.log(nilai + " " + "I will become a frontend developer");
  }
  nilai--;
}

//soal 2
console.log("\n=======\nSOAL 2\n=======");

for (var i = 1; i <= 20; i++) {
  if (i % 3 == 0 && i % 2 != 0) {
    console.log(i + " I love coding");
  } else if (i % 2 != 0) {
    console.log(i + " Santai");
  } else if (i % 2 == 0) {
    console.log(i + " Berkualitas");
  }
}

//soal 3
console.log("\n=======\nSOAL 3\n=======");

for (var n = "#"; n.length <= 7; n = n + "#") console.log(n);

//soal 4
console.log("\n=======\nSOAL 4\n=======");

var kalimat = "saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

//soal 4
console.log("\n=======\nSOAL 5\n=======");

var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];

daftarBuah.sort();
for (var i = 0; i < daftarBuah.length; i++) {
  console.log(daftarBuah[i]);
}
