// soal 1
console.log("SOAL 1");
console.log("------\n");

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(
  kataPertama
    .concat(" ")
    .concat(kataKedua[0].toUpperCase())
    .concat(kataKedua.substring(1))
    .concat(" ")
    .concat(kataKetiga)
    .concat(" ")
    .concat(kataKeempat.toUpperCase())
);
console.log("=================================\n");

// soal 2
console.log("SOAL 2");
console.log("------\n");

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var kataPertamaInt = Number(kataPertama);
var kataKeduaInt = Number(kataKedua);
var kataKetigaInt = Number(kataKetiga);
var kataKeempatInt = Number(kataKeempat);

console.log(kataPertamaInt + kataKeduaInt + kataKetigaInt + kataKeempatInt);
console.log("=================================\n");

// soal 3
console.log("SOAL 3");
console.log("------\n");

var kalimat = "wah javascipt itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 13);
var kataKetiga = kalimat.substring(14, 17);
var kataKeempat = kalimat.substring(18, 23);
var kataKelima = kalimat.substring(24, 32);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);

console.log("=================================\n");

// soal 4
console.log("SOAL 4");
console.log("------\n");

var nilai = 100;

if (nilai < 50) {
  console.log("E");
} else if (nilai < 60) {
  console.log("D");
} else if (nilai < 70) {
  console.log("C");
} else if (nilai < 80) {
  console.log("B");
} else if (nilai <= 100) {
  console.log("A");
} else {
  console.log("Nilai hanya 0 sampai 100");
}

console.log("=================================\n");

// soal 5
console.log("SOAL 5");
console.log("------\n");

var tanggal = 15;
var bulan = 6;
var tahun = 1994;

switch (bulan) {
  case 1: {
    bulan = "Januari";
    break;
  }
  case 2: {
    bulan = "Februari";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "April";
    break;
  }
  case 5: {
    bulan = "Mei";
    break;
  }
  case 6: {
    bulan = "Juni";
    break;
  }
  case 7: {
    bulan = "Juli";
    break;
  }
  case 8: {
    bulan = "Agustus";
    break;
  }
  case 9: {
    bulan = "September";
    break;
  }
  case 10: {
    bulan = "Oktober";
    break;
  }
  case 11: {
    bulan = "November";
    break;
  }
  case 12: {
    bulan = "Desember";
    break;
  }
  default: {
    bulan = "Tidak termasuk angka bulan";
  }
}

console.log(tanggal + " " + bulan + " " + tahun);
console.log("=================================\n");
