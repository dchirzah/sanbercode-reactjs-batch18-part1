//soal 1
console.log("\n=========== SOAL 1 ===========");

//fungsi halo
function halo() {
  return "Halo Sanbers!";
}

console.log(halo()); // "Halo Sanbers!"

//soal 2
console.log("\n=========== SOAL 2 ===========");

//fungsi kalikan
function kalikan(a, b) {
  return a * b;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

//soal 3
console.log("\n=========== SOAL 3 ===========");

function introduce(a, b, c, d) {
  return (
    "Nama saya " +
    a +
    ", umur saya " +
    b +
    " tahun, alamat saya di " +
    c +
    ", dan saya punya hobby yaitu " +
    d +
    "!"
  );
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"

//soal 4
console.log("\n=========== SOAL 4 ===========");

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
//objek mengisi manual
var objDaftarPeserta = {
  nama: "Asep",
  "jenis kelamin": "laki-laki",
  hobi: "baca buku",
  "tahun lahir": 1992,
};
//objek mengambil dari array
var objDaftarPeserta1 = {
  nama: arrayDaftarPeserta[0],
  "jenis kelamin": arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  "tahun lahir": arrayDaftarPeserta[3],
};
//menampilkan data dari array
console.log(
  "Nama saya " +
    arrayDaftarPeserta[0] +
    ", saya seorang " +
    arrayDaftarPeserta[1] +
    ", hobi saya " +
    arrayDaftarPeserta[2] +
    ", dan saya lahir tahun " +
    arrayDaftarPeserta[3]
);
//menampilkan data dari objek
console.log(
  "Nama saya " +
    objDaftarPeserta.nama +
    ", saya seorang " +
    objDaftarPeserta["jenis kelamin"] +
    ", hobi saya " +
    objDaftarPeserta.hobi +
    ", dan saya lahir tahun " +
    objDaftarPeserta["tahun lahir"]
);
//menampilkan objek dari data array
console.log(objDaftarPeserta1);

//soal 5
console.log("\n=========== SOAL 5 ===========");

//objek daftarBuah
var daftarBuah = [
  {
    nama: "strawberry",
    warna: "merah",
    "ada bijinya": "tidak",
    harga: 9000,
  },
  {
    nama: "jeruk",
    warna: "oranye",
    "ada bijinya": "ada",
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    "ada bijinya": "tidak",
    harga: 5000,
  },
];

//menampilkan isi objek daftarBuah pertama
console.log(daftarBuah[0]);

//soal 6
console.log("\n=========== SOAL 6 ===========");

//deklarasi array dataFilm
var dataFilm = [];

//fungsi addFilm untuk menambahkan data film
function addFilm(nama, durasi, genre, tahun) {
  dataFilm.push({
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  }); //menambahkan data ke dalam array
}
addFilm("spiderman", "2 jam 10 menit", "scifi", 2018); //input data pertama
addFilm("Big Hero 6", "2 jam 30 menit", "anime", 2017); //input data kedua

console.log(dataFilm); //menampilkan isi array
