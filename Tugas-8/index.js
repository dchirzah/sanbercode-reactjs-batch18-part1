// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

//Soal 1
console.log("---------- SOAL 1 ----------");

//menggunakan hell
let time = 10000;
/*readBooks(time, books[0], sisa => {
    readBooks(sisa, books[1], sisa1 => {
        readBooks(sisa1, books[2], sisa2 =>
            readBooks(sisa2, books[3], sisa3=> {
                return sisa3;
            }))
    })
}) */

//menggunakan fungsi rekursif
let i = 0;

let execute = (time) => {
  readBooks(time, books[i], (sisa) => {
    if (sisa !== 0) {
      i++;
      execute(sisa);
    }
  });
};
execute(time);
