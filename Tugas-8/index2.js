var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise

//Soal 2
console.log("---------- SOAL 2 ----------");
let i = 0;
time = 10000;
let execute = (time) => {
  readBooksPromise(time, books[i])
    .then((sisa) => {
      if (sisa !== 0) {
        i++;
        execute(sisa);
      }
    })
    .catch((error) => {
      console.log("selesai membaca buku");
    });
};

/* function execute(time) {
  readBooksPromise(time, books[i])
    .then(function (sisa) {
      if (sisa !== 0) {
        i++;
        execute(sisa);
      }
    })
    .catch(function (error) {
      console.log("selesai membaca");
    });
} */
execute(time);
