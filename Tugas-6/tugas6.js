//soal 1
console.log(
  "================================== SOAL 1 =================================="
);

const luasLingkaran = (r) => {
  let phi = 3.14;
  return phi * (r * r);
};

const kelilingLingkaran = (r) => {
  let phi = 3.14;
  return 2 * phi * r;
};

//Driver code
console.log("Luas lingkaran r=7 adalah :" + luasLingkaran(14));
console.log("Keliling lingkaran dengan r=14 adalah : " + kelilingLingkaran(14));

//soal 2
console.log(
  "================================== SOAL 2 =================================="
);

let kalimat = "";

const tambahKata = (kata) => {
  kalimat += `${kata} `;
};

//Driver code
tambahKata("saya");
tambahKata("adalah");
tambahKata("seorang");
tambahKata("frontend");
tambahKata("developer");
console.log(kalimat);

//soal 3
console.log(
  "================================== SOAL 3 =================================="
);

const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(`${firstName} ${lastName}`);
    },
  };
};
//Driver code
newFunction("William", "Imoh").fullName();
//soal 4
console.log(
  "================================== SOAL 4 =================================="
);

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation, spell } = newObject;
//Driver code
console.log(firstName, lastName, destination, occupation, spell);

//soal 5
console.log(
  "================================== SOAL 5 =================================="
);

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

//Driver code
const combined = [...west, ...east];
console.log(combined);
